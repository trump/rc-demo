/**
 * Created by trump on 15/4/11.
 */
var packageCfg = require('../package.json');
var exports = module.exports = {};

exports.knownAPI = {
    login: '/account/login'
    , logout: '/account/logout'
    , register: '/account/register'

    , taskList: '/tasks'
    , taskAdd: '/tasks/add'
    , taskRemove: '/tasks/remove'
    , taskEdit: '/tasks/update'
};

exports.knownAPIUrl = {};

exports.knownKeys = {
   isLogin: 'account_isLogin'
    , token: 'account_token'
};

Object.keys(exports.knownAPI).forEach(function(key){
    exports.knownAPIUrl[key] = packageCfg.apiRoot + exports.knownAPI[key];
});