/**
 * Created by trump on 15/4/5.
 */
var frameModule = require('ui/frame');
var exports = module.exports;

var viewModel;

exports.pageLoaded = function(args) {
    var page = args.object;

};

exports.save = function(args) {
    viewModel.save();
    frameModule.topmost().goBack();
};

exports.cancel = function() {
    frameModule.topmost().goBack();
};

exports.navigatedTo = function(args) {
    var page = args.object;
    viewModel = page.navigationContext;
    page.bindingContext = viewModel;
};