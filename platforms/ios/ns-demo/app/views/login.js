/**
 * Created by trump on 15/4/5.
 */
var frameModule = require('ui/frame');
var accountModel = require('../view-models/accountViewModel');
var exports = module.exports;

exports.pageLoaded = function(args) {
    var page = args.object;
    page.bindingContext = accountModel;
};

exports.loginTap = function () {
    accountModel.isLogin = true;
    frameModule.topmost().goBack();
};

exports.registerTap = function () {
    alert('wait for implementation.')
};

exports.goBack = function() {
    frame.topmost().goBack();
};