/**
 * Created by trump on 15/4/11.
 */
var http = require('http');
var helper = require('./helper');
module.exports = {
    login: function(accountModel, next) {
        return http.request({
            url: helper.knownAPIUrl.login
            , method: 'post'
            , content: JSON.stringify( accountModel )
        }).then(function(res){
            next(null, res);
        }, function(err){
            next(err);
        });
    }
    , logout: function(next) {
        return http.request({
            url: helper.knownAPIUrl.logout
            , method: 'post'
        }).then(function(res){
            next(null, res);
        }, function(err){
            next(err);
        });
    }
};