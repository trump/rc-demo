var observableModule = require("data/observable");
var Observable = observableModule.Observable;

__extend(ViewModel, Observable);

function ViewModel() {
    Observable.apply(this, arguments);
    this._isLogin = false;
    this._userName = '';
    this._password = '';
}

Object.defineProperty(ViewModel.prototype, 'isLogin', {
    get: function() {
        return this._isLogin;
    }
    , set: function(value) {
        if(this._isLogin !== value){
            this._isLogin = value;
            this.notify({
                object: this
                , eventName: observableModule.knownEvents.propertyChange
                , propertyName: "isLogin"
                , value: value
            });
        }
    }
});

Object.defineProperty(ViewModel.prototype, 'userName', {
    get: function() {
        return this._userName;
    }
    , set: function(value) {
        if(this._userName !== value){
            this._userName = value;
            this.notify({
                object: this
                , eventName: observableModule.knownEvents.propertyChange
                , propertyName: "userName"
                , value: value
            });
        }
    }
});

Object.defineProperty(ViewModel.prototype, 'password', {
    get: function() {
        return this._password;
    }
    , set: function(value) {
        if(this._password !== value){
            this._password = value;
            this.notify({
                object: this
                , eventName: observableModule.knownEvents.propertyChange
                , propertyName: "password"
                , value: value
            });
        }
    }
});

module.exports = new ViewModel();
