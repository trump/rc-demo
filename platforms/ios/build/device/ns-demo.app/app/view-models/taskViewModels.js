/**
 * Created by trump on 15/4/5.
 */

var observableModule = require("data/observable");
var Observable = observableModule.Observable;

var observableArrayModule = require("data/observable-array");

__extend(TaskViewManagers, Observable);

function TaskViewManagers() {
    Observable.apply(this, arguments);
}

TaskViewManagers.prototype.loadTasks = function() {
    var data = [
        { name: 'NativeScript Introduce' }
        , { name: 'NativeScript Setup' }
        , { name: 'NativeScript Demo' }
        , { name: 'NativeScript Thank you' }
    ].map(function(item){
            return new TaskItem(item);
        });

    var array = new observableArrayModule.ObservableArray(data);

    this.set('tasks', array);
};

TaskViewManagers.prototype.addTask = function(task) {
    this.getTaskList().push(task);
};

TaskViewManagers.prototype.updateTask = function(index, task) {
    this.getTaskList().setItem(index, task);
};

TaskViewManagers.prototype.getTaskList = function(){
    var tasks = this.get('tasks');
    if(!tasks) {
        this.set('tasks', new observableArrayModule.ObservableArray([]));
        tasks = this.get('tasks');
    }
    return tasks;
};

__extend(TaskItem, Observable);
function TaskItem(item){
    Observable.apply(this, arguments);
    var self = this;
    Object.keys(item).forEach(function(k){
        self.set(k, item[k]);
    });
}

module.exports  = {
    taskManager: new TaskViewManagers()
    , TaskItem: TaskItem
};
