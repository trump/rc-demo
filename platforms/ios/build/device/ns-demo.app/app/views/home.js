/**
 * Created by trump on 15/4/5.
 */
var frame = require('ui/frame');
var taskViewModule = require('../view-models/taskViewModels');
var taskViewManager = taskViewModule.taskManager;
var observableModule = require("data/observable");

var accountModel = require('../view-models/accountViewModel');

var buttonModule = require("ui/button");

var exports = module.exports;

var counter = 0;

exports.pageLoaded = function(args) {
    var page = args.object;
    var button = page.content.getViewById('AccountLoginLogout');
    button.text = accountModel.isLogin? 'Logout' : 'Login';

    if(counter++ ==0 ) {
        page.bindingContext = taskViewManager;
        taskViewManager.loadTasks();

        button.on(buttonModule.knownEvents.tap, function (args) {
            // Do something
            if(accountModel.isLogin){
                exports.logout();
            }else{
                exports.login();
            }
        });
    }

};

exports.navigatedTo = function(args) {

};

exports.login = function() {
    frame.topmost().navigate('app/views/login');
};

exports.logout = function() {
    alert('logout');
};

exports.addTask = function(args) {
    var task = new taskViewModule.TaskItem({
        name: ''
        , tasks: ''
        , id: ''
    });
    frame.topmost().navigate({
        moduleName: 'app/views/taskEdit'
        , context: {
            task: task
            , save: function() {
                taskViewManager.addTask(this.task);
            }
        }
    });
};

exports.listViewItemTap = function(args) {
    var task = args.view.bindingContext;
    frame.topmost().navigate({
        moduleName: 'app/views/taskEdit'
        , context: {
            task: task
            , save: function() {
                taskViewManager.updateTask(args.index, this.task);
            }
        }
    });
};