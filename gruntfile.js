var path = require('path');
var exec = require('child_process').exec;
var avq = require('avril.queue');
var fs = require('fs-extra');

module.exports = function (grunt) {

    grunt.initConfig({
        watch: {
            app: {
                files: ["app/**/*.*"]
                , tasks: ['nativescript-dev']
                , options: { interrupt: true }
            }
        }
        , 'nativescript-dev': {
            default_options: {
                root: process.cwd()
                , platforms: [
                    'ios'
                    , 'android'
                ]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-watch");

    grunt.registerMultiTask('nativescript-dev', 'a grunt task for NativeScript project live reload', function () {
        var done = this.async();

        var mainQueue = avq();

        mainQueue.func(function(){
            console.log('file change begin auto rebuild at: ', new Date());
        });

        var rootDir = this.data && this.data.root || process.cwd();

        this.data.platforms.forEach(buildPlatform);

        mainQueue.func(function() {
            console.log('finish live reload.')
            done();
        });

        function buildPlatform(platform) {
            var q = avq();
            //q.func(function(){ console.log('begin preparing: ', platform);  });
            //q.$await(exec, 'cd ' + rootDir + ' & ' + 'tns prepare ' + platform);

            q.func(function(){ console.log('copying files') });
            q.$await(fs.remove, path.join( rootDir, 'android/app/src/main/assets/app') );

            q.$await(fs.copy
                , path.join( rootDir, 'app/app')
                , path.join( rootDir, 'android/app/src/main/assets/app') )

            q.$await(fs.copy
                , path.join( rootDir, 'platforms/android/libs')
                , path.join( rootDir, 'android/app/libs') );

            q.$await(fs.remove, path.join( rootDir, 'platforms/android/assets/app') );
            q.$await(fs.remove, path.join(rootDir, 'platforms/ios/cailan.ajax.so/app' ));


            //q.func(function(){ console.log('begin building: ', platform);  });
            //q.$await(exec, 'cd ' + rootDir + ' & ' + 'tns build ' + platform);

            //q.func(function(){ console.log('begin running: ', platform);  });
            //q.$paralAwait(exec, 'cd ' + rootDir + ' & ' + 'tns run ' + platform);

            q.func(function(){ console.log('begin emulating: ', platform);  });
            q.$paralAwait(exec, 'cd ' + rootDir + ' & ' + 'tns emulate ' + platform);

            q.func(function(){ console.log('everything done on :', platform) });

            mainQueue.func(function(next) {
                q.func(function(){
                    next();
                });
            });
        }
    });

    grunt.registerTask("default", ["watch"]);
};